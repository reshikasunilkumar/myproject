import React,{ Component } from 'react';
import './App.css';
import Person from './Person/Person';

class App extends Component {
  state = {
    persons : [
      {name:'Reshika', age:22},
      {name:'Sreya', age:23},
      {name:'Aiswarya', age:24}
    ]
  }
  switchNameHandler = () => {
    //console.log('Button clicked');
    this.setState({
      persons : [
        {name:'Reshika Sunilkumar', age:22},
        {name:'Sreya P V', age:23},
        {name:'Aishu', age:25}
      ]
    });
  };
  nameChangedHandler = (event) =>{
    this.setState({
      persons : [
        {name:'Reshika Sunilkumar', age:22},
        {name:event.target.value, age:23},
        {name:'Aishu', age:25}
      ]
    });
  }
  render(){
    //inline style
    const style = {
      backgroundColor: '#ccc',
      padding: '10px',
      border : '2px solid #eee',
      boxShadow: '0 2px 2px #14171C',
      pointer: 'cursor'
    }
  return (
    <div className="App">
      <h1>Welcome to this page</h1>
      <p>I'm a React App</p>
      <button style={style}
      onClick={ this.switchNameHandler }>Switch name</button>
      <Person 
        name={ this.state.persons[0].name } 
        age={ this.state.persons[0].age } 
      />
      <Person 
        name={ this.state.persons[1].name } 
        age={this.state.persons[1].age } 
        click={this.switchNameHandler}
        changed={this.nameChangedHandler}
      />
      <Person 
        name={ this.state.persons[2].name } 
        age={ this.state.persons[2].age }
        >My hobbies are dancing and singing
        </Person>
    </div>
  );
  }
}
export default App;
