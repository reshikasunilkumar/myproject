import React from 'react';
import './Person.css';
const person = (props) => {
    return (
        <div className='Person'>
            <p onClick={props.click}>Hello I'm {props.name} and I'm {props.age} years old {props.children}</p>
            <input type="text" onChange={props.changed}></input>
        </div>
    )
};

export default person;
