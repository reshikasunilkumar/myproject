import React from 'react';
import './UserOutput.css';
const userOutput = (props) => {
    return(
        <div className='output'>
            <p>This is user output</p>
            <p>Username : { props.userName}</p>
        </div>
    );
};

export default userOutput;