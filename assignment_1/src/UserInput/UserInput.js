import React from 'react';

const userInput = (props) => {
    const style = {
        float: 'left',
        margin: '30px',
        border: '2px solid #ccc',
        boxShadow: '0 2px 2px rgb(117, 131, 155)',
        padding: '10px'
      }
    return <input type="text" style={style}
    onChange={props.changed}
    value={props.currentName}/>;
};

export default userInput;