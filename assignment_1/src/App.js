import React, { Component} from 'react';
import './App.css';
import UserInput from './UserInput/UserInput';
import UserOutput from './UserOutput/UserOutput';

class App extends Component{
  state = {
    username: 'eyescristal'
  }
  
  userNameChangeHandler = (event) => {
    this.setState({
      username: event.target.value
    });
  };
  render(){
    return(
      <div className='App'>
        <h5>Assignment</h5>
        <h6>Create 2 components UserInput and UserOutput</h6>
        
        <UserInput changed={this.userNameChangeHandler}
        currentName={this.state.username}
        />
        <UserOutput userName={ this.state.username } />
        <UserOutput userName='sreyapv' />
        <UserOutput userName='aishu' />
      </div>
    );
  }
  

}

export default App;
